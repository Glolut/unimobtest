using System.Collections.Generic;
using UnityEngine;

namespace GamePlay.Scripts.Utils.Item
{
    public abstract class ItemManager : MonoBehaviour
    {
        [SerializeField] protected OneItem defaultSelectedItem;
        protected List<OneItem> _allItems;
        public OneItem currentItem { get; protected set; }

        protected virtual void Start()
        {
            if (defaultSelectedItem) OnSelectItem(defaultSelectedItem);
        }

        public virtual void Subscribe(OneItem oneItem)
        {
            _allItems ??= new List<OneItem>();
            _allItems.Add(oneItem);
            oneItem.itemManager = this;
        }

        public virtual void OnSelectItem(OneItem oneItem)
        {
            if (currentItem == oneItem) return;
            currentItem = oneItem;
        }
    }
}