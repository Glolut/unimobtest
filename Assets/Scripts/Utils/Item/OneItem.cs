using UnityEngine;
using UnityEngine.EventSystems;
using Utils.Item;

namespace GamePlay.Scripts.Utils.Item
{
    public abstract class OneItem : MonoBehaviour, IPointerClickHandler
    {
        public ItemManager itemManager;

        protected virtual void Awake()
        {
            if (itemManager)
                Subscribe(itemManager);
        }

        public virtual void Subscribe(ItemManager itmMng)
        {
            itemManager.Subscribe(this);
        }


        public virtual void OnPointerClick(PointerEventData eventData)
        {
            itemManager.OnSelectItem(this);
        }

        public void Unsubscribe()
        {
            Destroy(gameObject);
        }
    }
}