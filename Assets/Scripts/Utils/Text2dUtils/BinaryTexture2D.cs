using System;
using UnityEngine;

namespace Utils.Item.Scripts.Utils.Text2dUtils
{
    public class BinaryTexture2D : MonoBehaviour
    {
        public static byte[] Base64ToBytes(string base64)
        {
            if (string.IsNullOrEmpty(base64))
                return Array.Empty<byte>();
            var bytes = Convert.FromBase64String(base64);
            return bytes;
        }

        public static Texture2D BytesToTexture2D(byte[] bytes)
        {
            Texture2D texture = new Texture2D(1024, 1024, TextureFormat.ETC2_RGBA8Crunched, false);
            texture.LoadImage(bytes);
            texture.Apply();
            return texture;
        }

        public static string BytesToBase64(byte[] bytes)
        {
            if (bytes.Length <= 0)
                return null;
            var base64 = Convert.ToBase64String(bytes);
            return base64;
        }

        public static Texture2D Base64ToTexture2D(string base64)
        {
            return BytesToTexture2D(Base64ToBytes(base64));
        }
    }
}