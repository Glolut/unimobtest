using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace PaintStatue.Scripts.Utils.Tab
{
    public abstract class Tab : MonoBehaviour, IPointerClickHandler
    {
        [FormerlySerializedAs("bagTabController")]
        public TabController tabController;

        public GameObject linkedPage;


        protected virtual void Awake()
        {
            tabController.Subscribe(this);
        }

        public virtual void ActiveTab()
        {
            linkedPage.SetActive(true);
        }

        public UnityEvent deActiveTabEvent;

        public virtual void DeActiveTab()
        {
            deActiveTabEvent?.Invoke();
            linkedPage.SetActive(false);
        }

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            tabController.OnSelectTab(this);
        }
    }
}