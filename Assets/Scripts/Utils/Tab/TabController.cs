using System;
using System.Collections.Generic;
using UnityEngine;

namespace PaintStatue.Scripts.Utils.Tab
{
    public abstract class TabController : MonoBehaviour
    {
        [SerializeField] protected Tab defaultTab;
        protected List<Tab> Tabs;
        public Tab CurrentTab { get; private set; }

        protected virtual void Start()
        {
            OnSelectTab(defaultTab);
        }

        public virtual void Subscribe(Tab bagTab)
        {
            //Set current selected tab
            bagTab.tabController = this;
            //add tab to list
            Tabs ??= new List<Tab>();
            Tabs.Add(bagTab);
        }

        public virtual void OnSelectTab(Tab tab)
        {
            if (CurrentTab == tab) return;
            CurrentTab = tab;
            ShowCurrentTab(tab);
        }

        /// <summary>
        /// Show current tab and hide other tab
        /// </summary>
        protected virtual void ShowCurrentTab(Tab curTab)
        {
            Tab activeTab = null;
            foreach (Tab tab in Tabs) {
                if (tab == curTab)
                    activeTab = tab;
                else
                    tab.DeActiveTab();
            }

            if (activeTab == null)
                throw new ArgumentException("Tab to show is null");
            activeTab.ActiveTab(); //Disable all tab 1st then enable the selected tab;
        }
    }
}