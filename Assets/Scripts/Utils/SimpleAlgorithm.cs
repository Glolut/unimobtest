﻿namespace Utils
{
    public class SimpleAlgorithm
    {
        public void MinMaxArray(int[] arr, out int MIN, out int MAX)
        {
            int i, max, min, n;
            // size of the array
            n = 5;
            max = arr[0];
            min = arr[0];
            for (i = 1; i < n; i++)
            {
                if (arr[i] > max)
                {
                    max = arr[i];
                }

                if (arr[i] < min)
                {
                    min = arr[i];
                }
            }
            MIN = min;
            MAX = max;
        }
    }
}