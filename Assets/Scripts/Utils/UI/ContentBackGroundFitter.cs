using System;
using UnityEngine;
using UnityEngine.UI;

namespace Utils.Item.Scripts.Utils.UI
{
    /// <summary>
    /// This class use for Content of a scroll view with grid layout
    /// Making it resize to all elements
    /// But also always fit the View Port even there are only few elements
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(LayoutElement))]
    public class ContentBackGroundFitter : MonoBehaviour
    {
        [SerializeField] private RectTransform viewPort;

        private void Start()
        {
            LayoutElement layoutElement = GetComponent<LayoutElement>();
            layoutElement.minHeight = viewPort.rect.height;
            // layoutElement.preferredHeight = viewPort.rect.height;
        }
        
        // places rect transform to have the same dimensions as 'other', even if they don't have same parent.
        // Relatively non-expensive.
        // NOTICE - also modifies scale of your RectTransform to match the scale of other
        public void MatchOther(RectTransform rt, RectTransform other)
        {
            Vector2 myPrevPivot = rt.pivot;
            myPrevPivot = other.pivot;
            rt.position = other.position;

            rt.localScale = other.localScale;

            rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, other.rect.width);
            rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, other.rect.height);
            //rectTransf.ForceUpdateRectTransforms(); - needed before we adjust pivot a second time?
            rt.pivot = myPrevPivot;
        }
    }
}