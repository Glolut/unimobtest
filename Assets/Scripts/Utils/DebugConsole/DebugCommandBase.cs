using System;
using Unity.VisualScripting;

namespace Utils.Item.Scripts.Utils.DebugConsole
{
    public class DebugCommandBase
    {
        public string commandId { get; private set; }
        public string commandDescription { get; private set; }
        public string commandFormat { get; private set; }

        public DebugCommandBase(string commandId, string commandDescription, string commandFormat)
        {
            this.commandId = commandId;
            this.commandDescription = commandDescription;
            this.commandFormat = commandFormat;
        }
    }

    public class DebugCommand : DebugCommandBase
    {
        public Action command;

        public DebugCommand(string commandId, string commandDescription, string commandFormat, Action command) : base(
            commandId, commandDescription, commandFormat)
        {
            this.command = command;
        }

        public void Invoke()
        {
            command.Invoke();
        }
    }

    public class DebugCommand<T1> : DebugCommandBase
    {
        private Action<int> command;

        public DebugCommand(string commandId, string commandDescription, string commandFormat, Action<int> command) :
            base(commandId, commandDescription, commandFormat)
        {
            this.command = command;
        }

        public void Invoke(int value)
        {
            command.Invoke(value);
        }
    }

    public class DebugCommand<T1, T2> : DebugCommandBase
    {
        private Action<int, string> command;

        public DebugCommand(string commandId, string commandDescription, string commandFormat,
            Action<int, string> command) :
            base(commandId, commandDescription, commandFormat)
        {
            this.command = command;
        }

        public void Invoke(int value1, string value2)
        {
            command.Invoke(value1, value2);
        }
    }
}