using UnityEngine;

namespace Utils
{
#if UNITY_EDITOR
    public class Screenshot : MonoBehaviour
    {
        public int count;
        private void Update()
        {
            if (!Input.GetKeyDown(KeyCode.S)) return;
            ScreenCapture.CaptureScreenshot("Screenshot/screen" + count + ".png");
            count++;
        }
    }
#endif
}