using UnityEngine;
using UnityEngine.UI;

public class ShowFPS : MonoBehaviour
{
    private float timer, refresh, avgFramerate;
    private string display = "{0} FPS";
    [SerializeField] private Text targetDisplay;

    private float timelapse;
    // Update is called once per frame
    void Update()
    {
        timelapse = Time.smoothDeltaTime;
        timer = timer <= 0 ? refresh : timer -= timelapse;
        
        if (timer <= 0) avgFramerate = (int) (1f/timelapse);
        targetDisplay.text = string.Format(display, avgFramerate.ToString());
    }
}
