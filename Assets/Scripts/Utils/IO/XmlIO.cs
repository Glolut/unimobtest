﻿using System;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace Utils.Item.Scripts.Utils
{
    /// <summary>
    /// Save and Load Xml file
    /// </summary>
    public static class XmlIO
    {
        private static XmlSerializer _xml;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="obj"></param>
        /// <typeparam name="T"></typeparam>
        public static void SetPath<T>(string path, T obj)
        {
            if (string.IsNullOrEmpty(path) || obj == null)
                return;
            _xml = new XmlSerializer(typeof(T));
            FileStream stream = new FileStream(Application.dataPath + path + ".xml", FileMode.Create);
            _xml.Serialize(stream, obj);
            stream.Close();
        }

        /// <summary>
        /// Save file
        /// </summary>
        /// <param name="path">File name</param>
        /// <param name="obj">Class</param>
        /// <typeparam name="T">Class</typeparam>
        public static void SetPlatform<T>(string path, T obj)
        {
            if (string.IsNullOrEmpty(path) || obj == null)
                return;
            _xml = new XmlSerializer(typeof(T));
            FileStream stream = new FileStream(Tools.GetPath() + path + ".xml", FileMode.Create);
            _xml.Serialize(stream, obj);
            stream.Close();
        }

        /// <summary>
        /// Check if path is existed
        /// </summary>
        /// <param name="path"></param>
        /// <returns>true if exist</returns>
        /// <exception cref="ArgumentException"></exception>
        public static bool CheckPlatform(string path)
        {
            if (string.IsNullOrEmpty(path))
                throw new ArgumentException("Path is null");
            return File.Exists(Tools.GetPath() + path + ".xml");
        }

        public static bool CheckPlatformFolder(string path)
        {
            if (string.IsNullOrEmpty(path))
                throw new ArgumentException("Path is null");
            return File.Exists(Tools.GetPath() + path);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetPath<T>(string path) where T : class, new()
        {
            if (string.IsNullOrEmpty(path))
                return null;
            _xml = new XmlSerializer(typeof(T));
            T obj = new T();
            if (!File.Exists(Application.dataPath + path + ".xml"))
                return obj;
            FileStream stream = new FileStream(Application.dataPath + path + ".xml", FileMode.Open);
            obj = _xml.Deserialize(stream) as T;
            stream.Close();

            return obj;
        }

        /// <summary>
        /// Get file, if file not exist, return obj with constructor has no parameter
        /// </summary>
        /// <param name="path"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetData<T>(string path) where T : class, new()
        {
            if (string.IsNullOrEmpty(path))
                return null;
            _xml = new XmlSerializer(typeof(T));
            T obj = new T();
            if (!File.Exists(Tools.GetPath() + path + ".xml"))
                throw new ArgumentException("File not found: " + Tools.GetPath() + path + ".xml");
            FileStream stream = new FileStream(Tools.GetPath() + path + ".xml", FileMode.Open);
            obj = _xml.Deserialize(stream) as T;
            stream.Close();
            return obj;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetResources<T>(string path) where T : class, new()
        {
            if (string.IsNullOrEmpty(path))
                return null;
            _xml = new XmlSerializer(typeof(T));
            T obj = new T();
            if (!File.Exists(Application.dataPath + "/Resources/" + path + ".xml"))
                return obj;
            TextAsset textAsset = Resources.Load<TextAsset>(path);
            if (textAsset == null)
                return obj;
            TextReader textReader = new StringReader(textAsset.ToString());
            obj = _xml.Deserialize(textReader) as T;
            textReader.Close();
            return obj;
        }
    }
}