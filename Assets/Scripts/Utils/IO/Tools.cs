﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public enum Platform
{
    Resources,
    Android,
    Ios,
    Else
}

public class Tools
{
    public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0)
    {
        Debug.DrawLine(start, end, color, duration);
    }

    public static void DrawRay(Vector3 start, Vector3 end, Color color, float duration = 0)
    {
        Debug.DrawRay(start, end, color, duration);
    }

    public static void Log(string log)
    {
        Debug.Log(log);
    }

    public static void LogWarning(string log)
    {
        Debug.LogWarning(log);
    }

    public static void LogError(string log)
    {
        Debug.LogError(log);
    }

    public static void LogException(Exception e)
    {
        Debug.LogException(e);
    }

    public static void BytesToFile(string path, byte[] bytes)
    {
        File.WriteAllBytes(path, bytes);
    }

    public static object BytesToObject(byte[] bytes)
    {
        if (bytes.Length <= 0)
            return null;
        MemoryStream stream = new MemoryStream(bytes);
        BinaryFormatter formatter = new BinaryFormatter();
        return formatter.Deserialize(stream);
    }

    public static byte[] ObjectToBytes(object obj)
    {
        if (obj == null)
            return new byte[0];
        MemoryStream stream = new MemoryStream();
        BinaryFormatter formatter = new BinaryFormatter();
        formatter.Serialize(stream, obj);
        return stream.ToArray();
    }

    public static byte[] Base64ToBytes(string base64)
    {
        if (string.IsNullOrEmpty(base64))
            return new byte[0];
        var bytes = Convert.FromBase64String(base64);
        return bytes;
    }

    public static string BytesToBase64(byte[] bytes)
    {
        if (bytes.Length <= 0)
            return null;
        var base64 = Convert.ToBase64String(bytes);
        return base64;
    }

    public static Platform GetPlatform()
    {
#if UNITY_EDITOR
        const Platform platform = Platform.Resources;
#elif UNITY_ANDROID
        const Platform platform = Platform.Android;
#elif UNITY_IOS
        const Platform platform = Platform.Ios;
#else
        const Platform platform = Platform.Else;
#endif
        return platform;
    }

    public static string GetPath()
    {
        Platform platform = GetPlatform();
        string path;
        switch (platform)
        {
            case Platform.Resources:
                path = Application.dataPath + "/Resources/";
                break;
            case Platform.Android:
                path = Application.persistentDataPath + "/";
                break;
            case Platform.Ios:
                path = Application.persistentDataPath + "/";
                break;
            case Platform.Else:
                path = Application.dataPath + "/";
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        return path;
    }

    public static string HexToBinary(string hex)
    {
        var binarys = new string[hex.Length];
        for (var i = 0; i < hex.Length - 1; i++)
        {
            var c = hex[i];
            switch (c)
            {
                case '0':
                    binarys[i] = "0000";
                    break;
                case '1':
                    binarys[i] = "0001";
                    break;
                case '2':
                    binarys[i] = "0010";
                    break;
                case '3':
                    binarys[i] = "0011";
                    break;
                case '4':
                    binarys[i] = "0100";
                    break;
                case '5':
                    binarys[i] = "0101";
                    break;
                case '6':
                    binarys[i] = "0110";
                    break;
                case '7':
                    binarys[i] = "0111";
                    break;
                case '8':
                    binarys[i] = "1000";
                    break;
                case '9':
                    binarys[i] = "1001";
                    break;
                case 'A':
                case 'a':
                    binarys[i] = "1010";
                    break;
                case 'B':
                case 'b':
                    binarys[i] = "1011";
                    break;
                case 'C':
                case 'c':
                    binarys[i] = "1100";
                    break;
                case 'D':
                case 'd':
                    binarys[i] = "1101";
                    break;
                case 'E':
                case 'e':
                    binarys[i] = "1110";
                    break;
                case 'F':
                case 'f':
                    binarys[i] = "1111";
                    break;
            }
        }

        var binary = binarys.Aggregate("", (current, t) => current + t);

        return binary;
    }

    public static int BinaryToDecimal(string binary)
    {
        return Convert.ToInt32(binary, 2);
    }
}