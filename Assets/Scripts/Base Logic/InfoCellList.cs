using UnityEngine;

namespace Game_Manager.Scriptable_Object
{
    [CreateAssetMenu(fileName = "Center cell list", menuName = "Scriptable Object/Center Cell List")]
    public sealed class InfoCellList : ScriptableObject
    {
        //Each element contains the center corresponding to a cell
        public Vector3[,] CenterCellList;

        //Each element counts the number of enemy inside it
        public int[,] CountInvaderPerCell;
    }
}