using System.Collections.Generic;
using UnityEngine;

namespace Base_Logic
{
    public class Spawner : MonoBehaviour
    {
        private Spider tempSpider;
        private int tempX;
        private int tempY;
        private int tempColor;
        
        public List<RuntimeAnimatorController> colorAnimations;

        private void Start()
        {
            if (Application.platform == RuntimePlatform.Android)
                Application.targetFrameRate = 60;
            else
                Application.targetFrameRate = -1;
        }
        private void SpawnRandom()
        {
            tempSpider = SpiderManager.instance.GetSpider();
            tempX = Random.Range(0, 10);
            tempY = Random.Range(0, 8);
            tempColor = Random.Range(0, colorAnimations.Count);
            
            tempSpider.SetColor(colorAnimations[tempColor]);
            tempSpider.transform.position = FlowField.instance.GetPosition(tempX, tempY);
            tempSpider.OnSpawn();
        }

        public void SpawnGivenOfNumber(int count)
        {
            for (int i = 0; i < count; i++) {
                SpawnRandom();
            }
        }
        
    }
}
