using System;
using System.Drawing;
using Base_Logic;
using UnityEngine;
using UnityEngine.Pool;
using Grid = Base_Logic.Grid;

public enum MyAnimationState
{
    None,
    Left,
    Right,
    Down,
    Up
}
public sealed class Spider : MonoBehaviour
{
    [SerializeField] private bool canUseTeleport;
    [SerializeField] private Transform thisTransform;
    [HideInInspector] public bool isFinalCell;
    [SerializeField] private Animator animator;

    private float Speed = 3f;
    private Point TargetCell;
    private Grid.Vector _vectorTemp;
    private Vector3 direction;

    private Vector3 Position;
    private int IDCell;
    private bool CalledOnReachFinalCell;

    public event Action OnTeleport;
    
    private IObjectPool<Spider> Pool;
    public void SetPool(IObjectPool<Spider> pool) => Pool = pool;

    // public void Start()
    // {
    //     OnSpawn();
    // }

    public void FixedUpdate()
    {
        //If invaders reached last cell. Release this prefab (return to pool)
        if (isFinalCell)
        {
            Pool.Release(this);
            return;
        }

        // if (canUseTeleport)
        //     CanTeleport();
        // else
        NoTeleport();
    }

    public void OnSpawn()
    {
        if (canUseTeleport && FlowField.instance.teleportIncluded) {
            TargetCell = FlowField.instance.teleportGrid.GetXY(thisTransform.position);
            SeekCellTargetOnSpawnTeleport();
        } else {
            TargetCell = FlowField.instance.basicGrid.GetXY(thisTransform.position);
            SeekCellTargetOnSpawn();
        }
    }

    private void SeekCellTargetOnSpawn()
    {
        (isFinalCell, TargetCell, _vectorTemp) = FlowField.instance.basicGrid.GetNextCell(TargetCell.X, TargetCell.Y);
        SetAnimation();
        // if (isFinalCell) return;
        // thisTransform.LookAt(FlowField.instance.infoCellListSo.CenterCellList[TargetCell.X, TargetCell.Y]);
    }

    private void SeekCellTargetOnSpawnTeleport()
    {
        if (FlowField.instance.teleportGrid.GetVector(TargetCell) == Grid.Vector.Teleport) {
            thisTransform.LookAt(FlowField.instance.infoCellListSo.CenterCellList[TargetCell.X, TargetCell.Y]);
        } else {
            (isFinalCell, TargetCell, _vectorTemp) =
                FlowField.instance.teleportGrid.GetNextCell(TargetCell.X, TargetCell.Y);
            if (isFinalCell) return;
            thisTransform.LookAt(FlowField.instance.infoCellListSo.CenterCellList[TargetCell.X, TargetCell.Y]);
        }
    }


    private void NoTeleport()
    {
        // try {
        //     IDCell = FlowField.instance.GetCellValue(TargetCell);
        // }
        // catch (Exception) {
        //     Pool.Release(this);
        //     Debug.Log("Kill " + name + " because it cannot find a path.");
        //     return;
        // }

        //When moving to a cell, if it is placed by a tower, detect current cell and move again
        if (IDCell == 1) {
            Position = thisTransform.position;
            TargetCell = FlowField.instance.GetXY(Position);
            SwitchCellTarget();

            Position = Vector3.MoveTowards(Position,
                FlowField.instance.infoCellListSo.CenterCellList[TargetCell.X, TargetCell.Y],
                Time.fixedDeltaTime);

            thisTransform.position = Position;
            return;
        }

        //=======Move to the target cell=========
        //1. Check face direction
        // direction = infoCellListSo.CenterCellList[TargetCell.X, TargetCell.Y] - thisTransform.position;
        //
        // if (Vector3.Angle(thisTransform.forward, direction) > 1f) {
        //     targetRotation = Quaternion.LookRotation(direction);
        //     thisTransform.rotation =
        //         Quaternion.RotateTowards(thisTransform.rotation, targetRotation,
        //             TurnRateDegreePS * Time.fixedDeltaTime);
        // }

        //2. When the the character face the moving direction, it can move now
        // If u want to make invader to face the direction 1st then it can go ==> uncomment the "else"
        // else
        thisTransform.position = Vector3.MoveTowards(thisTransform.position,
            FlowField.instance.infoCellListSo.CenterCellList[TargetCell.X, TargetCell.Y],
            Time.fixedDeltaTime * Speed);

        //If its reached a target cell, find new target cell
        if (thisTransform.position == FlowField.instance.infoCellListSo.CenterCellList[TargetCell.X, TargetCell.Y]) {
            SwitchCellTarget();
        }
    }

    private MyAnimationState _animationState;
    private void SwitchCellTarget()
    {
        (isFinalCell, TargetCell, _vectorTemp) = FlowField.instance.basicGrid.GetNextCell(TargetCell.X, TargetCell.Y);
        // Face the character to the direction moving
        // if (isFinalCell) return;
        //     thisTransform.LookAt(FlowField.instance.infoCellListSo.CenterCellList[TargetCell.X, TargetCell.Y]);
        SetAnimation();
    }

    private void SetAnimation()
    {
        switch (_vectorTemp) {

            case Grid.Vector.Up:
                if (_animationState != MyAnimationState.Up) {
                    animator.SetTrigger("Up");
                    _animationState = MyAnimationState.Up;
                }
                break;
            case Grid.Vector.Down:
                if (_animationState != MyAnimationState.Down) {
                    animator.SetTrigger("Down");
                    _animationState = MyAnimationState.Down;
                }
                break;
            case Grid.Vector.Left:
                if (_animationState != MyAnimationState.Left) {
                    animator.SetTrigger("Left");
                    _animationState = MyAnimationState.Left;
                }
                break;
            case Grid.Vector.Right:
                if (_animationState != MyAnimationState.Right) {
                    animator.SetTrigger("Right");
                    _animationState = MyAnimationState.Right;
                }
                break;
        }
    }

    public void SetColor(RuntimeAnimatorController animatorController)
    {
        animator.runtimeAnimatorController = animatorController;
    }
}