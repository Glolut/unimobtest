using System.Collections.Generic;
using System.Drawing;
using TMPro;
using UnityEngine;
using Utils;

namespace Base_Logic
{
    public sealed class GridTextDebug : Singleton<GridTextDebug>
    {
        public bool drawGridText;

        public List<GameObject> texts;
        private bool toggle = true;
        public void DrawTextCell(Point cellID)
        {
            //Setting up;
            if(!drawGridText) return;
            var textCell = new GameObject();
            texts.Add(textCell);
            var textMeshPro = textCell.AddComponent<TextMeshPro>();
            textMeshPro.fontSize = 3;
            var rect = textCell.GetComponent<RectTransform>();
            rect.sizeDelta = Vector2.one;
            rect.eulerAngles = new Vector3(90, 0, 0);

            var position = FlowField.instance.infoCellListSo.CenterCellList[cellID.X, cellID.Y] +
                           new Vector3(0, 0.05f, 0);
            rect.position = position;

            textMeshPro.text = cellID.X + "-" + cellID.Y;
            textMeshPro.alignment = TextAlignmentOptions.Center;
        }

        public void ToggleText()
        {
            toggle = !toggle;
            foreach (var e in texts) {
                e.gameObject.SetActive(toggle);
            }
        }
    }
}