using System;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using Color = UnityEngine.Color;
using Random = System.Random;

namespace Base_Logic
{
    [Serializable]
    public sealed class Grid
    {
        public enum Vector
        {
            None,
            Up,
            Down,
            Left,
            Right,
            Start,
            Teleport
        }

        private bool _includeTeleport;

        // length is always even, telepot entrance-exit always go as a pair -> (0-1), (2-3), ...
        //Note: teleport is 2-way entrance and exit.
        private Point[] _teleportCell;

        private float _cellSize;

        //0:blank cell (allow to place tower here)
        //1:tower placed 
        //2:object placed(wall, building,...)
        //3:no object but cannot place tower (mine, teleport, spawn point, ...) ==> invader can go to this cell
        private int[,] _gridArray;
        public Vector[,] CameFrom;

        //these temp are use for supposing place a tower in a cell before actually place tower to that cell
        private int[,]
            _gridArrayTemp;

        public Vector[,] CameFromTemp;

        private Vector3 _originPosition;

        public Point Start { get; }

        private bool _isDebug = true;


        public Grid(int[,] gridArrayTemp)
        {
            _gridArrayTemp = gridArrayTemp;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="cellSize"></param>
        /// <param name="originPosition"> Where the 1st cell start</param>
        /// <param name="start"></param>
        public Grid(int width, int height, float cellSize, Vector3 originPosition, Point start)
        {
            Width = width;
            Height = height;
            _cellSize = cellSize;
            _gridArray = new int[width, height];
            CameFrom = new Vector[width, height];

            _gridArrayTemp = new int[width, height];
            CameFromTemp = new Vector[width, height];
            _originPosition = originPosition;
            Start = start;
            if (_isDebug)
            {
                DrawCellDebug();
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="cellSize"></param>
        /// <param name="originPosition"> Where the 1st cell start</param>
        /// <param name="start"></param>
        /// <param name="teleport"></param>
        public Grid(int width, int height, float cellSize, Vector3 originPosition, Point start, Point[] teleport)
        {
            _includeTeleport = true;

            Width = width;
            Height = height;
            _cellSize = cellSize;
            _gridArray = new int[width, height];
            CameFrom = new Vector[width, height];

            _gridArrayTemp = new int[width, height];
            CameFromTemp = new Vector[width, height];
            _originPosition = originPosition;
            Start = start;
            _teleportCell = teleport;
            if (_isDebug)
            {
                DrawCellDebug();
            }
        }


        /// <summary>
        /// Return the cell size of the grid
        /// </summary>
        /// <returns></returns>
        public float GetCellSize()
        {
            return _cellSize;
        }

        /// <summary>
        /// Return the grid position in world space
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Vector3 GetWorldPosition(int x, int y)
        {
            return new Vector3(x, 0, y) * _cellSize + _originPosition;
        }

        private readonly Vector3 _constV3 = new(1, 0, 1);

        /// <summary>
        /// Return the center position of the give cell
        /// 0:blank cell (allow to place tower here)
        /// 1:tower placed 
        /// 2:object placed(wall, building,...)
        /// 3:no object but cannot place tower (mine, teleport, spawn point, ...) ==> invader can go to this cell
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Vector3 GetCenterPosition(int x, int y)
        {
            return GetWorldPosition(x, y) + _constV3 * _cellSize / 2f;
        }

        private Point _cac;

        public Vector3 GetCenterPosition(Vector3 worldPosition)
        {
            _cac = GetXY(worldPosition);
            return GetWorldPosition(_cac.X, _cac.Y) + Vector3.one * _cellSize / 2f;
        }

        /// <summary>
        /// Get the id of the cell that contains the given world position
        /// </summary>
        /// <param name="worldPosition"></param>
        /// <returns></returns>
        public Point GetXY(Vector3 worldPosition)
        {
            return new Point(Mathf.FloorToInt((worldPosition - _originPosition).x / _cellSize),
                Mathf.FloorToInt((worldPosition - _originPosition).z / _cellSize)
            );
        }

        /// <summary>
        /// Draw grid for debug (green line)
        /// </summary>
        public void DrawCellDebug()
        {
            for (var x = 0; x < Width; x++)
            {
                for (var y = 0; y < Height; y++)
                {
                    Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x, y + 1), Color.green, Mathf.Infinity);
                    Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x + 1, y), Color.green, Mathf.Infinity);
                }
            }

            Debug.DrawLine(GetWorldPosition(0, Height), GetWorldPosition(Width, Height), Color.green, 100f);
            Debug.DrawLine(GetWorldPosition(Width, 0), GetWorldPosition(Width, Height), Color.green, 100f);
        }

        /// <summary>
        /// Draw text of each cell
        /// </summary>
        public void DrawTextDebug()
        {
            if (!GridTextDebug.IsInstanceAlive) return;
            for (var x = 0; x < Width; x++)
            {
                for (var y = 0; y < Height; y++)
                {
                    GridTextDebug.instance.DrawTextCell(new Point(x, y));
                }
            }
        }

        public int Width { get; }
        public int Height { get; }

        /// <summary>
        /// Set value with ID cell
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="value">0:blank cell (allow to place tower here)1:tower placed 2:object placed(wall, building,...) 3:no object but cannot place tower (mine, teleport, spawn point, ...) ==> invader can go to this cell</param>
        public void SetValue(int x, int y, int value)
        {
            _gridArray[x, y] = value;
        }

        public void SetValueTemp(int x, int y, int value)
        {
            _gridArrayTemp[x, y] = value;
        }

        /// <summary>
        /// Set value with given world position
        /// </summary>
        /// <param name="position"></param>
        /// <param name="value">0 nothing, 1 is already place, 2 not placed able</param>
        public void SetValue(Vector3 position, int value)
        {
            var id = GetXY(position);
            _gridArray[id.X, id.Y] = value;
        }

        public void SetValueTemp(Vector3 position, int value)
        {
            var id = GetXY(position);
            _gridArrayTemp[id.X, id.Y] = value;
        }

        /// <summary>
        /// Get value of the given cell
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public int GetValue(int x, int y)
        {
            return _gridArray[x, y];
        }

        /// <summary>
        /// Get value with given world position
        /// </summary>
        /// <param name="position"></param>
        public int GetValue(Vector3 position)
        {
            var id = GetXY(position);
            return _gridArray[id.X, id.Y];
        }

        /// <summary>
        /// Check if cell is inside grid
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsInBound(Point id)
        {
            return id.X >= 0 && id.X < Width && id.Y >= 0 && id.Y < Height;
        }

        public Point GetRandomCell()
        {
            var r = new Random();
            var x = r.Next(_gridArray.GetLength(0));
            var y = r.Next(_gridArray.GetLength(1));
            return new Point(x, y);
        }

        /// <summary>
        /// Draw flow field
        /// </summary>
        /// <param name="x">id of the destination</param>
        /// <param name="y">id of the destination</param>
        public void FlowField(int x, int y)
        {
            //x and y are id of the cell
            var frontier = new Queue<Point>();
            frontier.Enqueue(new Point(x, y));
            CameFrom = new Vector[Width, Height];
            CameFrom[x, y] = Vector.Start;

            Point currentCell;
            Point tempNeighbor;
            //run this code is grid has no teleport
            if (!_includeTeleport)
            {
                while (frontier.Count != 0)
                {
                    currentCell = frontier.Dequeue();
                    //Preview 4 neighbors
                    tempNeighbor = new Point(currentCell.X + 1, currentCell.Y);
                    CheckAndAddNeighbor(tempNeighbor, frontier, Vector.Left);

                    tempNeighbor = new Point(currentCell.X - 1, currentCell.Y);
                    CheckAndAddNeighbor(tempNeighbor, frontier, Vector.Right);

                    tempNeighbor = new Point(currentCell.X, currentCell.Y - 1);
                    CheckAndAddNeighbor(tempNeighbor, frontier, Vector.Up);

                    tempNeighbor = new Point(currentCell.X, currentCell.Y + 1);
                    CheckAndAddNeighbor(tempNeighbor, frontier, Vector.Down);
                }
            }
            //run this code if grid including teleport
            else
            {
                while (frontier.Count != 0)
                {
                    currentCell = frontier.Dequeue();
                    //Preview 4 neighbors
                    tempNeighbor = new Point(currentCell.X + 1, currentCell.Y);
                    CheckAndAddNeighbor(tempNeighbor, frontier, Vector.Left);

                    tempNeighbor = new Point(currentCell.X - 1, currentCell.Y);
                    CheckAndAddNeighbor(tempNeighbor, frontier, Vector.Right);

                    tempNeighbor = new Point(currentCell.X, currentCell.Y - 1);
                    CheckAndAddNeighbor(tempNeighbor, frontier, Vector.Up);

                    tempNeighbor = new Point(currentCell.X, currentCell.Y + 1);
                    CheckAndAddNeighbor(tempNeighbor, frontier, Vector.Down);

                    IsCellTeleport(currentCell, frontier);
                }
            }
        }

        private void CheckAndAddNeighbor(Point tempNeighbor, Queue<Point> frontier, Vector vector)
        {
            if (!IsInBound(tempNeighbor)) return; //Check if cell is not inside the grid
            if (CameFrom[tempNeighbor.X, tempNeighbor.Y] != Vector.None ||
                CameFrom[tempNeighbor.X, tempNeighbor.Y] == Vector.Start) return;
            if (_gridArray[tempNeighbor.X, tempNeighbor.Y] != 0 &&
                _gridArray[tempNeighbor.X, tempNeighbor.Y] != 3) return;
            CameFrom[tempNeighbor.X, tempNeighbor.Y] = vector;
            frontier.Enqueue(tempNeighbor);
        }

        private void IsCellTeleport(Point cellID, Queue<Point> frontier)
        {
            var canTeleport = false;
            var index = -1;
            for (var i = 0; i < _teleportCell.Length; i++)
            {
                if (cellID == _teleportCell[i])
                {
                    canTeleport = true;
                    index = i;
                    break;
                }
            }

            if (canTeleport == false) return;
            //Teleport
            if (index % 2 == 0) CheckAndAddNeighbor(_teleportCell[index + 1], frontier, Vector.Teleport);
            else CheckAndAddNeighbor(_teleportCell[index - 1], frontier, Vector.Teleport);
        }

        /// <summary>
        /// Create a copy of flow field and set value of given cell to 1 (placed object)
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void FlowFieldTemp(int x, int y)
        {
            //Debug
            //for (var i = 0; i < Width; i++)
            // {
            //     for (var j = 0; j < Height; j++)
            //     {
            //         Debug.Log(_gridArray[i, j]);
            //     }
            // }
            CameFromTemp = new Vector[Width, Height];
            //x and y are id of the cell
            var frontier = new Queue<Point>();
            frontier.Enqueue(new Point(x, y));
            CameFromTemp[x, y] = Vector.Start;

            while (frontier.Count != 0)
            {
                var currentCell = frontier.Dequeue();
                //Preview 4 neighbors
                var tempNeighbor = new Point(currentCell.X + 1, currentCell.Y);
                CheckAndAddNeighborTemp(tempNeighbor, frontier, Vector.Left);

                tempNeighbor = new Point(currentCell.X - 1, currentCell.Y);
                CheckAndAddNeighborTemp(tempNeighbor, frontier, Vector.Right);

                tempNeighbor = new Point(currentCell.X, currentCell.Y - 1);
                CheckAndAddNeighborTemp(tempNeighbor, frontier, Vector.Up);

                tempNeighbor = new Point(currentCell.X, currentCell.Y + 1);
                CheckAndAddNeighborTemp(tempNeighbor, frontier, Vector.Down);
            }
        }


        private void CheckAndAddNeighborTemp(Point tempNeighbor, Queue<Point> frontier, Vector vector)
        {
            if (!IsInBound(tempNeighbor)) return;
            if (CameFromTemp[tempNeighbor.X, tempNeighbor.Y] != Vector.None ||
                CameFromTemp[tempNeighbor.X, tempNeighbor.Y] == Vector.Start) return;
            if (_gridArrayTemp[tempNeighbor.X, tempNeighbor.Y] != 0 &&
                _gridArrayTemp[tempNeighbor.X, tempNeighbor.Y] != 3) return;
            CameFromTemp[tempNeighbor.X, tempNeighbor.Y] = vector;
            frontier.Enqueue(tempNeighbor);
        }

        /// <summary>
        /// Get the next cell id for the next move of enemies in given cell
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public (bool, Point, Vector) GetNextCell(int x, int y)
        {
            switch (CameFrom[x, y])
            {
                case Vector.Down:
                    return (false, new Point(x, y - 1), Vector.Down);
                case Vector.Left:
                    return (false, new Point(x - 1, y), Vector.Left);
                case Vector.Right:
                    return (false, new Point(x + 1, y), Vector.Right);
                case Vector.Up:
                    return (false, new Point(x, y + 1), Vector.Up);
                case Vector.Start:
                    return (true, Start, Vector.Start);
                case Vector.Teleport:
                    var cell = new Point(x, y);
                    for (int i = 0; i < _teleportCell.Length; i++)
                    {
                        if (cell == _teleportCell[i])
                        {
                            if (i % 2 == 0)
                                return (false, _teleportCell[i + 1], Vector.Teleport);
                            else
                                return (false, _teleportCell[i - 1], Vector.Teleport);
                        }
                    }

                    // Debug.Log("Error at get next cell teleport");
                    // return (false, new Point(-1000, -1000), Vector.None);
                    throw new ArgumentException("Error at get next cell teleport");
                default:
                    // Debug.Log("Error at GetNextCell, cell is none");
                    // return (false, new Point(-1000, -1000), Vector.None);
                    throw new ArgumentException("Error at GetNextCell, cell is none");
            }
        }

        /// <summary>
        /// Get the next move for enemy in given cell
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Vector3 GetNextWorldPosition(int x, int y)
        {
            switch (CameFrom[x, y])
            {
                case Vector.Down:
                    return GetWorldPosition(x, y - 1);
                case Vector.Left:
                    return GetWorldPosition(x - 1, y);
                case Vector.Right:
                    return GetWorldPosition(x + 1, y);
                case Vector.Up:
                    return GetWorldPosition(x, y + 1);
                default:
                    // Debug.Log("Error: Cannot find next cell of Start or None cell");
                    // return GetWorldPosition(-10000, -10000);
                    throw new ArgumentException("Cannot find next cell of Start or None cell");
            }
        }

        public Vector GetVector(Point cellID)
        {
            return CameFrom[cellID.X, cellID.Y];
        }

        public Vector GetVectorTemp(Point cellID)
        {
            return CameFromTemp[cellID.X, cellID.Y];
        }

        public Vector GetVectorTemp(int x, int y)
        {
            return CameFromTemp[x, y];
        }

        public void SetTemp()
        {
            _gridArrayTemp = (int[,])_gridArray.Clone();
            CameFromTemp = (Vector[,])CameFrom.Clone();
        }


        public Vector[,] CameFromAStar;

        /// <summary>
        /// Find closest path from a cell to a closest tower
        /// Return list of cell id  that lead to closest Tower (in order)
        /// </summary>
        /// <param name="x">ID' x of the cell</param>
        /// <param name="y">ID' y of the cell</param>
        public List<Point> AStarToClosestTower(int x, int y)
        {
            //x and y are id of the cell
            var frontier = new Queue<Point>();
            frontier.Enqueue(new Point(x, y));
            CameFromAStar = new Vector[Width, Height];
            try
            {
                CameFromAStar[x, y] = Vector.Start;
            }
            catch (Exception e)
            {
                Debug.Log("error at (x, y) = " + x + " " + y + "at Grid.cs");
            }


            Point currentCell = default;
            while (frontier.Count != 0)
            {
                currentCell = frontier.Dequeue();
                //check if currentCell is tower, then we done
                if (_gridArray[currentCell.X, currentCell.Y] == 1)
                    break;
                //Preview 4 neighbors
                var tempNeighbor = new Point(currentCell.X + 1, currentCell.Y);
                CheckAndAddNeighborAStar(tempNeighbor, frontier, Vector.Left);

                tempNeighbor = new Point(currentCell.X - 1, currentCell.Y);
                CheckAndAddNeighborAStar(tempNeighbor, frontier, Vector.Right);

                tempNeighbor = new Point(currentCell.X, currentCell.Y - 1);
                CheckAndAddNeighborAStar(tempNeighbor, frontier, Vector.Up);

                tempNeighbor = new Point(currentCell.X, currentCell.Y + 1);
                CheckAndAddNeighborAStar(tempNeighbor, frontier, Vector.Down);
            }

            var path = new List<Point> { new Point(currentCell.X, currentCell.Y) };
            while (true)
            {
                currentCell = CameFromAStar[currentCell.X, currentCell.Y] switch
                {
                    Vector.Up => new Point(currentCell.X, currentCell.Y + 1),
                    Vector.Down => new Point(currentCell.X, currentCell.Y - 1),
                    Vector.Left => new Point(currentCell.X - 1, currentCell.Y),
                    Vector.Right => new Point(currentCell.X + 1, currentCell.Y),
                    _ => currentCell
                };
                path.Add(new Point(currentCell.X, currentCell.Y));
                if (currentCell == new Point(x, y))
                    break;
            }

            path.Reverse();
            return path;
        }

        private void CheckAndAddNeighborAStar(Point tempNeighbor, Queue<Point> frontier, Vector vector)
        {
            if (!IsInBound(tempNeighbor)) return;
            if (CameFromAStar[tempNeighbor.X, tempNeighbor.Y] != Vector.None ||
                CameFromAStar[tempNeighbor.X, tempNeighbor.Y] == Vector.Start) return;

            if (_gridArray[tempNeighbor.X, tempNeighbor.Y] == 0 ||
                _gridArray[tempNeighbor.X, tempNeighbor.Y] == 1)
            {
                CameFromAStar[tempNeighbor.X, tempNeighbor.Y] = vector;
            }
            else
            {
                return;
            }

            frontier.Enqueue(tempNeighbor);
        }
    }
}