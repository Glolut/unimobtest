using UnityEngine;
using UnityEngine.Pool;
using Utils;

public class SpiderManager : Singleton<SpiderManager>
{
    [SerializeField] private Spider spiderPrefab;
    
    private ObjectPool<Spider> _spiderPool;
    
    
    protected override void Awake()
    {
        base.Awake();
        _spiderPool =
            new ObjectPool<Spider>(InstantiateSpider, OnTakeSpiderFromPool, OnReturnSpiderToPool);
    }
    private void OnReturnSpiderToPool(Spider obj)
    {
        obj.gameObject.SetActive(false);
    }
    private void OnTakeSpiderFromPool(Spider obj)
    {
        obj.gameObject.SetActive(true);
    }
    private Spider InstantiateSpider()
    {
        var spider = Instantiate(spiderPrefab);
        spider.SetPool(_spiderPool);
        return spider;
    }

    public Spider GetSpider()
    {
        return _spiderPool.Get();
    }
    
}
