using System;
using System.Collections.Generic;
using UnityEngine;

namespace Base_Logic
{
    [CreateAssetMenu(fileName = "MapCell", menuName = "Scriptable Object/MapCell")]
    public sealed class MapCell : ScriptableObject
    {
        // public Point[] GateLocation;
        public List<MyPoint> gateLocation;
        // public List<EngineerPath> EngineerPaths;
        // public List<AlliesEngineerPath> AlliesEngineerPaths;

        [Space] [Header("Grid Data")] public MyPoint gridSize;

        public MyPoint baseLocation;

        public Vector3 gridOriginPosition;

        [Tooltip("cell that cannot be move and placed tower")]
        public List<MyPoint> Cell2;

        [Tooltip("cell that cannot be placed tower or mine but invader can move on there")]
        public List<MyPoint> Cell3;

        public bool includeTeleport;

        public List<MyPoint> teleportGrid;
        // public 
    }

    [Serializable]
    public struct MyPoint
    {
        public int x;
        public int y;
    }

    [Serializable]
    public struct MyFloatPoint
    {
        public float x;
        public float y;
    }

    // [Serializable]
    // public class Path
    // {
    //     public List<MyPoint> listCell;
    //     public BuildingStateClass.Building _Building;
    // }

    // [Serializable]
    // public class EngineerPath
    // {
    //     public int gateID;
    //     public List<MyPoint> listCell;
    // }
    //
    // [Serializable]
    // public class AlliesEngineerPath
    // {
    //     public List<MyPoint> listCell;
    // }
}