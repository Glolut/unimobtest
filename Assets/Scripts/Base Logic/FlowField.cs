using System;
using System.Collections.Generic;
using System.Drawing;
using Game_Manager.Scriptable_Object;
using UnityEngine;
using Utils;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;


namespace Base_Logic
{
    public sealed class FlowField : Singleton<FlowField>
    {
        //Main grid draw path for enemy
        public Grid basicGrid;
        public Grid teleportGrid;

        //Temp grid for preview tower, if this grid has no path to final goal, then that preview tower not allowed to place in that cell
        [SerializeField] private Transform arrowPrefabs;
        public InfoCellList infoCellListSo;
        public MapCell mapCell;
        [HideInInspector] public bool teleportIncluded;


        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        public static Vector3 DesPosition; //equal base position
        private readonly List<GameObject> _allArrow = new List<GameObject>();


        protected override void Awake()
        {
            base.Awake();
            teleportIncluded = mapCell.includeTeleport;
            //create grid
            basicGrid = new Grid(mapCell.gridSize.x, mapCell.gridSize.y, 1f,
                mapCell.gridOriginPosition, new Point(mapCell.baseLocation.x, mapCell.baseLocation.y));
            CacheCenterPosition(mapCell.gridSize.x, mapCell.gridSize.y);
            basicGrid.DrawTextDebug();
            //Cache all center of all cell to a array to reduce calculation time later on
            // Set value 2 (not move and placed able)
            for (var i = 0; i < mapCell.Cell2.Count; i++)
            {
                basicGrid.SetValue(mapCell.Cell2[i].x, mapCell.Cell2[i].y, 2);
            }

            //Set Value 3 (not placeable but descendible)
            for (var i = 0; i < mapCell.Cell3.Count; i++)
            {
                basicGrid.SetValue(mapCell.Cell3[i].x, mapCell.Cell3[i].y, 3);
            }
            // for (var i = 0; i < 15; i++)
            // {
            //     basicGrid.SetValue(i, 0, 2);
            // }
            //
            // for (var i = 0; i < 15; i++)
            // {
            //     basicGrid.SetValue(i, 19, 2);
            // }

            //Set value 0 to override the 2 value (for spawn cell only, no need for base)
            DesPosition = infoCellListSo.CenterCellList[mapCell.baseLocation.x, mapCell.baseLocation.y];
            // DesPosition = infoCellListSo.CenterCellList[mapCell.baseLocation.x, mapCell.baseLocation.y];

            //create teleport grid
            // teleportGrid = new Grid(15, 20, 1f, new Vector3(-7.5f, 0, -10), new Point(10, 0),
            //     new[] { new Point(6, 16), new Point(9, 2) });
            if (!teleportIncluded) return;
            var teleportArray = new Point[mapCell.teleportGrid.Count];
            for (var i = 0; i < teleportArray.Length; i++)
            {
                teleportArray[i] = new Point(mapCell.teleportGrid[i].x, mapCell.teleportGrid[i].y);
            }

            teleportGrid = new Grid(mapCell.gridSize.x, mapCell.gridSize.y, 1f,
                mapCell.gridOriginPosition, new Point(mapCell.baseLocation.x, mapCell.baseLocation.y), teleportArray);

            for (var i = 0; i < mapCell.Cell2.Count; i++)
            {
                teleportGrid.SetValue(mapCell.Cell2[i].x, mapCell.Cell2[i].y, 2);
            }

            // for (var i = 0; i < 15; i++)
            // {
            //     teleportGrid.SetValue(i, 0, 2);
            // }
            //
            // for (var i = 0; i < 15; i++)
            // {
            //     teleportGrid.SetValue(i, 19, 2);
            // }

            // teleportGrid.SetValue(10, 19, 0);
        }

        private void Start()
        {
            CalculateFlowField();
            // DrawFlowField();
        }

        /// <summary>
        /// Calculate flow field (use to update flow field after place or remove tower)
        /// </summary>
        public void CalculateFlowField()
        {
            basicGrid.FlowField(mapCell.baseLocation.x, mapCell.baseLocation.y);
            if (teleportIncluded) teleportGrid.FlowField(mapCell.baseLocation.x, mapCell.baseLocation.y);
        }

        /// <summary>
        /// Using for both teleport and non-teleport invader
        /// </summary>
        public int GetCellValue(Point targetCell)
        {
            return basicGrid.GetValue(targetCell.X, targetCell.Y);
        }

        /// <summary>
        /// Using for both teleport and non-teleport invader
        /// </summary>
        public Point GetXY(Vector3 position)
        {
            return basicGrid.GetXY(position);
        }

        public Vector3 GetPosition(int x, int y)
        {
            return basicGrid.GetWorldPosition(x, y) + basicGrid.GetCellSize()/2f * Vector3.one;
        }
        
        
        /// <summary>
        /// Destroy all debug arrow flow field
        /// </summary>
        public void DestroyAllArrow()
        {
            foreach (var t in _allArrow)
            {
                Destroy(t);
            }

            _allArrow.Clear();
        }

        /// <summary>
        /// Draw visual debug arrow
        /// </summary>
        public void DrawFlowField()
        {
            // if (!AllScreen.instance.debug) return;
            for (var i = 0; i < basicGrid.Width; i++)
            {
                for (var j = 0; j < basicGrid.Height; j++)
                {
                    var tempCellCenter = basicGrid.GetCenterPosition(i, j);
                    tempCellCenter.y = 0.01f;
                    switch (basicGrid.CameFrom[i, j])
                    {
                        case Grid.Vector.Left:
                            _allArrow.Add(Instantiate(arrowPrefabs, tempCellCenter,
                                Quaternion.Euler(new Vector3(90, 180f, 0f))).gameObject);
                            break;
                        case Grid.Vector.Up:
                            _allArrow.Add(Instantiate(arrowPrefabs, tempCellCenter,
                                Quaternion.Euler(new Vector3(90, -90f, 0f))).gameObject);
                            break;
                        case Grid.Vector.Down:
                            _allArrow.Add(Instantiate(arrowPrefabs, tempCellCenter,
                                Quaternion.Euler(new Vector3(90, 90f, 0f))).gameObject);
                            break;
                        case Grid.Vector.Right:
                            _allArrow.Add(Instantiate(arrowPrefabs, tempCellCenter,
                                Quaternion.Euler(new Vector3(90, 0f, 0f))).gameObject);
                            break;
                        case Grid.Vector.Teleport:
                            _allArrow.Add(Instantiate(arrowPrefabs, tempCellCenter,
                                Quaternion.Euler(new Vector3(0, 0f, 0f))).gameObject);
                            break;
                    }
                }
            }
        }

        public void DrawFlowFieldTeleport()
        {
            // if (!AllScreen.instance.debug) return;
            for (var i = 0; i < teleportGrid.Width; i++)
            {
                for (var j = 0; j < teleportGrid.Height; j++)
                {
                    var tempCellCenter = teleportGrid.GetCenterPosition(i, j);
                    tempCellCenter.y = 0.01f;
                    switch (teleportGrid.CameFrom[i, j])
                    {
                        case Grid.Vector.Left:
                            _allArrow.Add(Instantiate(arrowPrefabs, tempCellCenter,
                                Quaternion.Euler(new Vector3(90, 180f, 0f))).gameObject);
                            break;
                        case Grid.Vector.Up:
                            _allArrow.Add(Instantiate(arrowPrefabs, tempCellCenter,
                                Quaternion.Euler(new Vector3(90, -90f, 0f))).gameObject);
                            break;
                        case Grid.Vector.Down:
                            _allArrow.Add(Instantiate(arrowPrefabs, tempCellCenter,
                                Quaternion.Euler(new Vector3(90, 90f, 0f))).gameObject);
                            break;
                        case Grid.Vector.Right:
                            _allArrow.Add(Instantiate(arrowPrefabs, tempCellCenter,
                                Quaternion.Euler(new Vector3(90, 0f, 0f))).gameObject);
                            break;
                        case Grid.Vector.Teleport:
                            _allArrow.Add(Instantiate(arrowPrefabs, tempCellCenter,
                                Quaternion.Euler(new Vector3(0, 0f, 0f))).gameObject);
                            break;
                        case Grid.Vector.None:
                            break;
                        case Grid.Vector.Start:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
        }

        /// <summary>
        /// Cache all center position of all cell on the map
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        private void CacheCenterPosition(int width, int height)
        {
            infoCellListSo.CenterCellList = new Vector3[width, height];
            infoCellListSo.CountInvaderPerCell = new int[width, height];
            for (var i = 0; i < basicGrid.Width; i++)
            {
                for (var j = 0; j < basicGrid.Height; j++)
                {
                    infoCellListSo.CenterCellList[i, j] = basicGrid.GetCenterPosition(i, j);
                }
            }
        }

        private Grid.Vector _vector;
        private Point _currentCell;
        private bool _lastCheck;

        /// <summary>
        /// Copy to gridTemp the current grid then placing a tower to given cell
        /// Check if path to final goal still exist
        /// TODO - check if its not block path from multi spawn point
        /// </summary>
        /// <param name="cellID">ID of the cell that object is placed on</param>
        /// <returns>Check if path to final goal still exist</returns>
        public bool IsObjectPlaceBlockPath(Point cellID)
        {
            basicGrid.SetTemp();
            basicGrid.SetValueTemp(cellID.X, cellID.Y, 1);
            basicGrid.FlowFieldTemp(mapCell.baseLocation.x, mapCell.baseLocation.y);
            //=====Check "Tower surround a cell" ======
            //Loop through all cell, if that cell has invaders, check if there is path for invaders in that cell
            for (var i = 0; i < basicGrid.Width; i++)
            {
                for (var j = 0; j < basicGrid.Height; j++)
                {
                    if (infoCellListSo.CountInvaderPerCell[i, j] <= 0) continue;
                    if (basicGrid.GetVectorTemp(i, j) == Grid.Vector.None)
                    {
                        // print("bucac at cell has invader");
                        return true;
                    }
                }
            }

            //=====Check path to final cell=====
            //only check 1 times a cell, if still that cell, no need to check again
            if (cellID == _currentCell) return _lastCheck;
            _currentCell = cellID;

            //Check at Base Point
            _vector = basicGrid.GetVectorTemp(basicGrid.GetXY(DesPosition));
            if (_vector == Grid.Vector.None)
            {
                _lastCheck = true;
                // print("bucac at check base point");
                return true;
            }

            //Check at Spawn Point (gate location)
            foreach (var gate in mapCell.gateLocation)
            {
                _vector = basicGrid.GetVectorTemp(new Point(gate.x, gate.y));
                if (_vector == Grid.Vector.None)
                {
                    _lastCheck = true;
                    // print("bucac at check at spawn point");
                    return true;
                }
            }

            _lastCheck = false;
            return false;
        }
    }
}